---
layout: page
title: About
permalink: /about/
---

Hi! I'm [Oğuz Kaan Yüksel](https://oguzkaanyuksel.com), a double major bachelor student in [Computer Engineering](https://www.cmpe.boun.edu.tr) and [Mathematics](http://www.math.boun.edu.tr) at [Boğaziçi University](http://www.boun.edu.tr). I have been working on adversarial machine learning for quite some time and I decided to construct this website to organize and share my research in the field.

The website will mostly include the following:

* Review of papers in the field with detailed commentary
* Wild experiments that I wrestle in a day-to-day basis
* In-depth analysis of some of the questions and themes in the field


Please note that this is an _open research_ project. It is likely that some of the stuff here will be unpublished. I might be a fool to assume that most of the curious minds of today will be seeking to _actively collobarate and advance this work together_ rather than copycating anything here, but I will assume that anyway because it feels much better! So, if you have any questions, ideas or you want to advance this work in any way just let me know! And please do excuse me if I am blathering at times as most of the work here will be uncomplete.
